# vue-ankr
`vue-ankr` is package providing a component based on the functions of the lightweight library
[`ankr`][ankr]; namely, `<Ankr/>` (see [CodeSandbox.io][csbx] for a demo).

The `<Ankr/>` component has several exposed `props` and an event:

```
<Ankr
  @ankr:click
  :nHorizontal
  :nVertical
  :snap
  :i
  :space
  :padding
  :margin
  :edgeOnly
>
  <slot>
    <!-- your content here -->
  </slot>
</Ankr>
```

## `events`
- `@ankr:click`: fires when the `<Ankr/>` component is clicked,

## `props`
- `:nHorizontal`: how many horizontal anchors
- `:nVertical`: how many vertical anchors
- `:edgeOnly`: whether or not anchors fill in the middle of the screen or go just around the edge
- `:snap`: whether or not the `<Ankr/>` component should "snap" to an anchor point after drag, or be freely draggable
- `:i`: which anchor point of `nHorizontal` times `nVertical` to start at
- `:space`: an array of length two specifying the `[width, height]` of the `boundingClientRect` of the element in the `<slot>`
- `:padding`: an array of length two specifying the `[padX, padY]` for the `<Ankr>`
- `:margin`: an array of length two specifying the `[marginX, marginY]` for the `<Ankr>`






[ankr]: https://www.npmjs.com/package/ankr
[csbx]: https://codesandbox.io/s/2z89l94y50
