(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('ankr')) :
  typeof define === 'function' && define.amd ? define(['exports', 'ankr'], factory) :
  (factory((global['vue-ankr'] = global['vue-ankr'] || {}),global.ankr));
}(this, (function (exports,ankr) { 'use strict';

  ankr = ankr && ankr.hasOwnProperty('default') ? ankr['default'] : ankr;

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  var script = {
    name: "Ankr",
    props: {
      nHorizontal: {
        default: ankr.HORIZONTAL_NUMBER_OF_ANCHORS
      },
      nVertical: {
        default: ankr.VERTICAL_NUMBER_OF_ANCHORS
      },
      snap: {
        default: true
      },
      i: {
        default: 0
      },
      space: {
        default: function _default() {
          return [0, 0];
        }
      },
      padding: {
        default: function _default() {
          return [0, 0];
        }
      },
      margin: {
        default: function _default() {
          return [0, 0];
        }
      },
      edgeOnly: {
        default: true
      }
    },
    data: function data() {
      return {
        x: 0,
        y: 0,
        drag: false,
        anchors: []
      };
    },
    computed: {
      style: function style() {
        var _ref = [this.i, [this.x, this.y]],
            i = _ref[0],
            _ref$ = _slicedToArray(_ref[1], 2),
            x = _ref$[0],
            y = _ref$[1];

        if (this.snap && !this.drag) {
          var _ankr$closestAnchor = ankr.closestAnchor([x, y], this.anchors);

          var _ankr$closestAnchor2 = _slicedToArray(_ankr$closestAnchor, 2);

          i = _ankr$closestAnchor2[0];

          var _ankr$closestAnchor2$ = _slicedToArray(_ankr$closestAnchor2[1], 2);

          x = _ankr$closestAnchor2$[0];
          y = _ankr$closestAnchor2$[1];
        }

        return {
          left: "".concat(x, "px"),
          top: "".concat(y, "px"),
          position: "fixed"
        };
      }
    },
    created: function created() {
      window.addEventListener("resize", this.snapToNearestAnchor);
      window.addEventListener("resize", this.calculateAnchors);
      this.calculateAnchors();
      var index = this.i < this.anchors.length ? this.i : 0;
      var anchor = this.anchors[index];
      this.x = anchor[0];
      this.y = anchor[1];
    },
    methods: {
      calculateAnchors: function calculateAnchors() {
        this.anchors = ankr.anchors({
          dimension: [ankr.pageWidth(), ankr.pageHeight()],
          space: this.space,
          padding: this.padding,
          margin: this.margin,
          anchors: [this.nHorizontal, this.nVertical],
          edgeOnly: this.edgeOnly
        });
      },
      snapToNearestAnchor: function snapToNearestAnchor() {
        var _ref2 = [0, [this.x, this.y]],
            i = _ref2[0],
            _ref2$ = _slicedToArray(_ref2[1], 2),
            x = _ref2$[0],
            y = _ref2$[1];

        var _ankr$closestAnchor3 = ankr.closestAnchor([x, y], this.anchors);

        var _ankr$closestAnchor4 = _slicedToArray(_ankr$closestAnchor3, 2);

        i = _ankr$closestAnchor4[0];

        var _ankr$closestAnchor4$ = _slicedToArray(_ankr$closestAnchor4[1], 2);

        x = _ankr$closestAnchor4$[0];
        y = _ankr$closestAnchor4$[1];
        var _ref3 = [i, [x, y]];
        this.i = _ref3[0];

        var _ref3$ = _slicedToArray(_ref3[1], 2);

        this.x = _ref3$[0];
        this.y = _ref3$[1];
      },
      handleMove: function handleMove(_ref4) {
        var pageX = _ref4.pageX,
            pageY = _ref4.pageY,
            clientX = _ref4.clientX,
            clientY = _ref4.clientY;

        if (this.$refs.ankr) {
          this.x = pageX + this.left;
          this.y = pageY + this.top;
          this.drag = true;
        }
      },
      handleDown: function handleDown(event) {
        var pageX = event.pageX,
            pageY = event.pageY;

        var _this$$refs$ankr$getB = this.$refs.ankr.getBoundingClientRect(),
            left = _this$$refs$ankr$getB.left,
            top = _this$$refs$ankr$getB.top;

        this.left = left - pageX;
        this.top = top - pageY;
        document.addEventListener("pointermove", this.handleMove);
      },
      handleUp: function handleUp(event) {
        var _this = this;

        document.removeEventListener("pointermove", this.handleMove);
        setTimeout(function () {
          return _this.drag = false;
        }, 50);
      },
      handleClick: function handleClick(event) {
        if (!this.drag) {
          //change click only if not draged
          this.click = !this.click;
          this.$emit("ankr:click", event);
        }
      }
    },
    beforeDestroy: function beforeDestroy() {
      window.removeEventListener("resize", this.snapToNearestAnchor);
      window.removeEventListener("resize", this.calculateAnchors);
    }
  };

  /* script */
              const __vue_script__ = script;
              
  /* template */
  var __vue_render__ = function() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", [
      _c(
        "div",
        {
          ref: "ankr",
          staticClass: "ankr",
          class: { "ankr--smooth-snap": _vm.snap && !_vm.drag },
          style: _vm.style,
          on: {
            pointerdown: _vm.handleDown,
            pointerup: _vm.handleUp,
            pointercancel: _vm.handleUp,
            click: _vm.handleClick
          }
        },
        [
          _c(
            "div",
            { staticClass: "ankr__wrapper" },
            [
              _vm._t("default", [
                _c("img", {
                  staticClass: "ankr-icon",
                  attrs: {
                    src:
                      "https://upload.wikimedia.org/wikipedia/commons/f/f1/Vue.png",
                    alt: "icon"
                  }
                })
              ])
            ],
            2
          )
        ]
      )
    ])
  };
  var __vue_staticRenderFns__ = [];
  __vue_render__._withStripped = true;

    /* style */
    const __vue_inject_styles__ = function (inject) {
      if (!inject) return
      inject("data-v-c3245996_0", { source: "\n.ankr__wrapper {\n  border: solid 1px transparent;\n  border-radius: 50%;\n  box-shadow: 0px 2px 10px #888888;\n  width: 50px;\n  height: 50px;\n}\n.ankr-icon {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  text-align: center;\n  margin: auto;\n  padding: 10px 0px;\n  width: 30px;\n  height: 30px;\n}\n.ankr__wrapper > * {\n  user-select: none;\n  -moz-user-select: none;\n  -webkit-user-select: none;\n  -ms-user-select: none;\n  pointer-events: none;\n}\n.ankr {\n  z-index: 1000;\n}\n.ankr--smooth-snap {\n  transition: all 1s ease;\n}\n", map: {"version":3,"sources":["/Users/sumner/Projects/vue-ankr/src/components/Ankr.vue"],"names":[],"mappings":";AA4IA;EACA,8BAAA;EACA,mBAAA;EACA,iCAAA;EACA,YAAA;EACA,aAAA;CACA;AAEA;EACA,eAAA;EACA,kBAAA;EACA,mBAAA;EACA,mBAAA;EACA,aAAA;EACA,kBAAA;EACA,YAAA;EACA,aAAA;CACA;AAEA;EACA,kBAAA;EACA,uBAAA;EACA,0BAAA;EACA,sBAAA;EACA,qBAAA;CACA;AAEA;EACA,cAAA;CACA;AAEA;EACA,wBAAA;CACA","file":"Ankr.vue","sourcesContent":["<template>\n  <div>\n    <div\n      ref=\"ankr\"\n\n      @pointerdown=\"handleDown\"\n      @pointerup=\"handleUp\"\n      @pointercancel=\"handleUp\"\n      @click=\"handleClick\"\n\n      class=\"ankr\"\n      :class=\"{ 'ankr--smooth-snap': snap && !drag }\"\n      :style=\"style\"\n    >\n      <div class=\"ankr__wrapper\">\n        <slot class=\"ankr__element\">\n          <img\n            src=\"https://upload.wikimedia.org/wikipedia/commons/f/f1/Vue.png\"\n            alt=\"icon\"\n            class=\"ankr-icon\"\n          />\n        </slot>\n      </div>\n    </div>\n  </div>\n</template>\n\n<script>\nimport ankr from \"ankr\";\n\nexport default {\n  name: \"Ankr\",\n  props: {\n    nHorizontal: {\n      default: ankr.HORIZONTAL_NUMBER_OF_ANCHORS\n    },\n    nVertical: {\n      default: ankr.VERTICAL_NUMBER_OF_ANCHORS\n    },\n    snap: {\n      default: true\n    },\n    i: {\n      default: 0\n    },\n    space: {\n      default: () => [0, 0]\n    },\n    padding: {\n      default: () => [0, 0]\n    },\n    margin: {\n      default: () => [0, 0]\n    },\n    edgeOnly: {\n      default: true\n    }\n  },\n  data() {\n    return {\n      x: 0,\n      y: 0,\n      drag: false,\n      anchors: []\n    };\n  },\n  computed: {\n    style() {\n      let [i, [x, y]] = [this.i, [this.x, this.y]];\n      if (this.snap && !this.drag) {\n        [i, [x, y]] = ankr.closestAnchor([x, y], this.anchors);\n      }\n      return {\n        left: `${x}px`,\n        top: `${y}px`,\n        position: \"fixed\"\n      };\n    }\n  },\n  created() {\n    window.addEventListener(\"resize\", this.snapToNearestAnchor);\n    window.addEventListener(\"resize\", this.calculateAnchors);\n    this.calculateAnchors();\n    let index = this.i < this.anchors.length ? this.i : 0;\n    let anchor = this.anchors[index];\n    this.x = anchor[0];\n    this.y = anchor[1];\n  },\n\n  methods: {\n    calculateAnchors() {\n      this.anchors = ankr.anchors({\n        dimension: [ankr.pageWidth(), ankr.pageHeight()],\n        space: this.space,\n        padding: this.padding,\n        margin: this.margin,\n        anchors: [this.nHorizontal, this.nVertical],\n        edgeOnly: this.edgeOnly\n      });\n    },\n    snapToNearestAnchor() {\n      let [i, [x, y]] = [0, [this.x, this.y]];\n      [i, [x, y]] = ankr.closestAnchor([x, y], this.anchors);\n      [this.i, [this.x, this.y]] = [i, [x, y]];\n    },\n    handleMove({ pageX, pageY, clientX, clientY }) {\n      if (this.$refs.ankr) {\n        this.x = pageX + this.left;\n        this.y = pageY + this.top;\n        this.drag = true;\n      }\n    },\n    handleDown(event) {\n      const { pageX, pageY } = event;\n      const { left, top } = this.$refs.ankr.getBoundingClientRect();\n\n      this.left = left - pageX;\n      this.top = top - pageY;\n      document.addEventListener(\"pointermove\", this.handleMove);\n    },\n    handleUp(event) {\n      document.removeEventListener(\"pointermove\", this.handleMove);\n      setTimeout(() => (this.drag = false), 50);\n    },\n    handleClick(event) {\n      if (!this.drag) {\n        //change click only if not draged\n        this.click = !this.click;\n        this.$emit(\"ankr:click\", event);\n      }\n    }\n  },\n  beforeDestroy() {\n    window.removeEventListener(\"resize\", this.snapToNearestAnchor);\n    window.removeEventListener(\"resize\", this.calculateAnchors);\n  }\n};\n</script>\n\n<style>\n.ankr__wrapper {\n  border: solid 1px transparent;\n  border-radius: 50%;\n  box-shadow: 0px 2px 10px #888888;\n  width: 50px;\n  height: 50px;\n}\n\n.ankr-icon {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  text-align: center;\n  margin: auto;\n  padding: 10px 0px;\n  width: 30px;\n  height: 30px;\n}\n\n.ankr__wrapper > * {\n  user-select: none;\n  -moz-user-select: none;\n  -webkit-user-select: none;\n  -ms-user-select: none;\n  pointer-events: none;\n}\n\n.ankr {\n  z-index: 1000;\n}\n\n.ankr--smooth-snap {\n  transition: all 1s ease;\n}\n</style>\n"]}, media: undefined });

    };
    /* scoped */
    const __vue_scope_id__ = undefined;
    /* module identifier */
    const __vue_module_identifier__ = undefined;
    /* functional template */
    const __vue_is_functional_template__ = false;
    /* component normalizer */
    function __vue_normalize__(
      template, style, script$$1,
      scope, functional, moduleIdentifier,
      createInjector, createInjectorSSR
    ) {
      const component = (typeof script$$1 === 'function' ? script$$1.options : script$$1) || {};

      // For security concerns, we use only base name in production mode.
      component.__file = "/Users/sumner/Projects/vue-ankr/src/components/Ankr.vue";

      if (!component.render) {
        component.render = template.render;
        component.staticRenderFns = template.staticRenderFns;
        component._compiled = true;

        if (functional) component.functional = true;
      }

      component._scopeId = scope;

      {
        let hook;
        if (style) {
          hook = function(context) {
            style.call(this, createInjector(context));
          };
        }

        if (hook !== undefined) {
          if (component.functional) {
            // register for functional component in vue file
            const originalRender = component.render;
            component.render = function renderWithStyleInjection(h, context) {
              hook.call(context);
              return originalRender(h, context)
            };
          } else {
            // inject component registration as beforeCreate hook
            const existing = component.beforeCreate;
            component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
          }
        }
      }

      return component
    }
    /* style inject */
    function __vue_create_injector__() {
      const head = document.head || document.getElementsByTagName('head')[0];
      const styles = __vue_create_injector__.styles || (__vue_create_injector__.styles = {});
      const isOldIE =
        typeof navigator !== 'undefined' &&
        /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

      return function addStyle(id, css) {
        if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

        const group = isOldIE ? css.media || 'default' : id;
        const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

        if (!style.ids.includes(id)) {
          let code = css.source;
          let index = style.ids.length;

          style.ids.push(id);

          if (isOldIE) {
            style.element = style.element || document.querySelector('style[data-group=' + group + ']');
          }

          if (!style.element) {
            const el = style.element = document.createElement('style');
            el.type = 'text/css';

            if (css.media) el.setAttribute('media', css.media);
            if (isOldIE) {
              el.setAttribute('data-group', group);
              el.setAttribute('data-next-index', '0');
            }

            head.appendChild(el);
          }

          if (isOldIE) {
            index = parseInt(style.element.getAttribute('data-next-index'));
            style.element.setAttribute('data-next-index', index + 1);
          }

          if (style.element.styleSheet) {
            style.parts.push(code);
            style.element.styleSheet.cssText = style.parts
              .filter(Boolean)
              .join('\n');
          } else {
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index]) style.element.removeChild(nodes[index]);
            if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
            else style.element.appendChild(textNode);
          }
        }
      }
    }
    /* style inject SSR */
    

    
    var Ankr = __vue_normalize__(
      { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
      __vue_inject_styles__,
      __vue_script__,
      __vue_scope_id__,
      __vue_is_functional_template__,
      __vue_module_identifier__,
      __vue_create_injector__,
      undefined
    );

  // NOTE: This setup uses 'babel-plugin-wildcard' to gather all .vue files
  var components = {
    Ankr: Ankr // install function executed by Vue.use()

  };

  function install(Vue) {
    if (install.installed) return;
    install.installed = true;
    Object.keys(components).forEach(function (componentName) {
      Vue.component(componentName, components[componentName]);
    });
  } // Create module definition for Vue.use()


  var plugin = {
    install: install
  }; // To auto-install when vue is found

  /* global window */

  var GlobalVue = null;

  if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
  } else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
  }

  if (GlobalVue) {
    GlobalVue.use(plugin);
  } // To allow use as module (npm/webpack/etc.) export component
  // also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
  // export const RollupDemoDirective = component;

  exports.Ankr = Ankr;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
